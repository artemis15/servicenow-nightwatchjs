# What is this?
  
  A way to test Servicenow with nightwatchjs

# How can I try it?

1.  Clone/download this repo on a windows machine

2.  Install NPM depencies

    `npm install`

3.  navigate to the bin directory run Selenium

    `java -jar selenium-server-standalone-3.0.1.jar`

4.  New Terminal/Command
    
    `node nightwatch.js -e chrome`

# Contributing
  
  See [CONTRIBUTING.MD](https://github.com/jacebenson/nightwatchjs-servicenow/blob/master/CONTRIBUTING.md)
